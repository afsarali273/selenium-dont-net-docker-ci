# .NET Core Selenium Tests with XUnit + Docker + Gitlab CI

A simple test implemented in .NET Core 3.1 that uses Selenium WebDriver with multiple browsers.

GitLab Continious Integration Pipeline is being set up , Automatic build will trigger when we will push some code changes to the branch.Altimately it will build the .Net Selenium with Xunit framework then Dockerize into a docker conatiner.

**Docker container will be having :**
1. Chrome Browser installed (Manual installed each time) [We can avoid this by using selenium grid docker image]
1. dotnet/sdk:3.1
1. Selenium 3.141
1. xunit
1. TRX reports (XML report) [TODO- Automated trx to html report generation]


```cmd
git clone https://gitlab.com/afsarali273/selenium-dont-net-docker-ci.git
cd Selenium-dont-net-Docker-ci
dotnet clean
dotnet build
dotnet test
```

```cmd
dotnet test --logger trx  // for test report

dotnet test --logger "console;verbosity=detailed"
```


**To Run from Docker file :**

Make sure you have docker installed in your machine
```bash
cd seleniumtests

docker build -t selenium-automation-image .

docker run -it selenium-automation-image:latest
```
