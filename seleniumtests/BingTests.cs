using System;
using System.Runtime.InteropServices;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using Xunit;

namespace seleniumtests
{
    public static class BingTests
    {
        [SkippableTheory]
        [InlineData("Chrome")]
        // [InlineData("Firefox")]
        public static void Search_For_DotNet_Core(string browserName)
        {
            // Arrange
            Skip.If(
                !RuntimeInformation.IsOSPlatform(OSPlatform.Windows) && browserName == "InternetExplorer",
                $"{browserName} is only supported on Windows.");



            // Act
            using var driver = CreateWebDriver(browserName);

            driver.Navigate().GoToUrl("https://demosite.executeautomation.com/Login.html");

            driver.FindElement(By.CssSelector("#userName > p:nth-child(1) > input[type=text]")).SendKeys("username");
            driver.FindElement(By.CssSelector("#userName > p:nth-child(2) > input[type=text]")).SendKeys("password");
            driver.FindElement(By.XPath("//*[@id='userName']/p[3]/input")).Click();

            Boolean isDisplayed = driver.FindElement(By.CssSelector("body > h1")).Displayed;

            Console.WriteLine("isSelected : " + isDisplayed);
            Console.WriteLine("Current Web Page Title : " + driver.Title);
            Console.WriteLine("Current Web Page URL : " + driver.Url);
            Console.WriteLine("\nCurrent Web Page Pagesource : \n" + driver.PageSource);


            Assert.True(isDisplayed);

        }

        private static IWebDriver CreateWebDriver(string browserName)
        {
            //string driverDirectory = System.IO.Path.GetDirectoryName(typeof(BingTests).Assembly.Location) ?? "."; //Uncomment while running locally
            string driverDirectory = "/opt/selenium/"; // For Docker only
            bool isDebuggerAttached = System.Diagnostics.Debugger.IsAttached;

            return browserName.ToLowerInvariant() switch
            {
                "chrome" => CreateChromeDriver(driverDirectory, isDebuggerAttached),

                "firefox" => CreateFirefoxDriver(driverDirectory, isDebuggerAttached),

                _ => throw new NotSupportedException($"The browser '{browserName}' is not supported."),
            };
        }

        private static IWebDriver CreateChromeDriver(
            string driverDirectory,
            bool isDebuggerAttached)
        {
            var options = new ChromeOptions();

            if (!isDebuggerAttached)
            {
                options.AddArgument("--headless");
            }


            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                options.AddArgument("--no-sandbox");
            }

            return new ChromeDriver(driverDirectory, options);
        }

        private static IWebDriver CreateFirefoxDriver(
            string driverDirectory,
            bool isDebuggerAttached)
        {
            var options = new FirefoxOptions();

            if (!isDebuggerAttached)
            {
                options.AddArgument("--headless");
            }

            return new FirefoxDriver(driverDirectory, options);
        }
    }
}
